﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUD_Entity.Models;

namespace CRUD_Entity.Controllers
{
    public class EntityController : Controller
    {
        EMPLOYEE_DETAILSEntities empObj = new EMPLOYEE_DETAILSEntities();
        
        // GET: Entity
        public ActionResult Index()
        {
            var result = empObj.tblEmployees.ToList();
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: /Employee/Create

        [HttpPost]

        public ActionResult Create(tblEmployee employeedetail)

        {



            empObj.tblEmployees.Add(employeedetail);
            empObj.SaveChanges();
            return View();

        }
        public ActionResult Edit(int id)

        {

            tblEmployee employeedetail = empObj.tblEmployees.Find(id);

            if (employeedetail == null)

            {

                return HttpNotFound();

            }

            return View(employeedetail);

        }

        //

        // POST: /Employee/Edit/5

        [HttpPost]

        public ActionResult Edit(tblEmployee employeedetail)

        {

            if (ModelState.IsValid)

            {

                empObj.Entry(employeedetail).State = EntityState.Modified;

                empObj.SaveChanges();

                return RedirectToAction("Index");

            }

            return View(employeedetail);

        }
    }
}