﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUD_Entity.Models;

namespace CRUD_Entity.Controllers
{
    public class EmployeeController : Controller
    {
        EMPLOYEE_DETAILSEntities1 emp = new EMPLOYEE_DETAILSEntities1();
        // GET: Employee
        public ActionResult Index()
        {
            var data = emp.Table_ADO.ToList();
            return View(data);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Table_ADO model)
        {
            if (ModelState.IsValid)
            {
                emp.Table_ADO.Add(model);
                emp.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);            
        }
        public ActionResult Edit(int id )
        {
            Table_ADO employeedetail = emp.Table_ADO.Find(id);
            if (employeedetail == null)
            {
                return HttpNotFound();
            }
            return View(employeedetail);

        }
        [HttpPost]

        public ActionResult Edit(Table_ADO employeedetail)
        {
            if (ModelState.IsValid)
            {
                emp.Entry(employeedetail).State = EntityState.Modified;
                emp.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employeedetail);
        }
        public ActionResult Details(int id)
        {
            Table_ADO employeedetail = emp.Table_ADO.Find(id);
            if (employeedetail == null)
            {
                return HttpNotFound();
            }
            return View(employeedetail);
        }

        public ActionResult Delete(int id = 0)
        {
            Table_ADO employeedetail = emp.Table_ADO.Find(id);
            if (employeedetail == null)
            {
                return HttpNotFound();
            }
            return View(employeedetail);
        }

        // POST: /Employee/Delete/5

        [HttpPost, ActionName("Delete")]

        public ActionResult DeleteConfirmed(int id)
        {

            Table_ADO employeedetail = emp.Table_ADO.Find(id);

            emp.Table_ADO.Remove(employeedetail);

            emp.SaveChanges();

            return RedirectToAction("Index");

        }

        //protected override void Dispose(bool disposing)

        //{

        //    emp.Dispose();

        //    base.Dispose(disposing);

        //}

    }

}

 
