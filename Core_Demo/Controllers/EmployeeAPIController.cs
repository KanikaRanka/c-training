﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Core_Demo.Models;
using Microsoft.AspNetCore.Mvc;

namespace Core_Demo.Controllers
{
    public class EmployeeAPIController : Controller
    {
        public IActionResult Index()
        {
            IEnumerable<Employee> emp = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50377/weatherforecast");
                var responseTask = client.GetAsync("weatherforecast");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readJob = result.Content.ReadAsAsync<IList<Employee>>();

                    readJob.Wait();
                    emp = readJob.Result;
                }
                else
                {
                    //return the error code here
                    emp = Enumerable.Empty<Employee>();
                    ModelState.AddModelError(string.Empty, "Error occured !");
                }

            }
            return View(emp);
        }
        public ActionResult Create()
        {
            return View();

        }
        [HttpPost]

        public ActionResult Create(Employee emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50377/weatherforecast");
                var postTask = client.PostAsJsonAsync<Employee>("weatherforecast", emp);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(emp);
            }
        }
        public ActionResult Edit(int id)
        {
            Employee model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50377/weatherforecast");
                var responseTask = client.GetAsync("weatherforecast/" + id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Employee>();
                    readTask.Wait();
                    model = readTask.Result;

                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(Employee emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50377/weatherforecast");
                var putTask = client.PutAsJsonAsync<Employee>("weatherforecast", emp);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(emp);
            }
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:50377/weatherforecast");
                var deleteTask = client.DeleteAsync("weatherforecast/" + id.ToString());
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }
    }
}