﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDemo.Models;

namespace WebApiDemo.Controllers
{
    public class EmployeeController : ApiController
    {
        EmployeeDBAccessLayer empDb = new EmployeeDBAccessLayer();
        public IHttpActionResult GetAllEmployee()
        {
            return Ok(empDb.GetEmployeeEntities());

        }
        public IHttpActionResult PostAddEmployee(EmployeeEntities modelObj)
        {
            empDb.AddEmployeeRecord(modelObj);
            return Ok();

        }
        public IHttpActionResult GetEditEmployee(int Id)
        {
            return Ok(empDb.GetEmployeeData(Id));
        }
        public IHttpActionResult PutUpdateEmployee(EmployeeEntities modelObj)
        {
            empDb.UpdateEmployee(modelObj);
            return Ok();
        }
        public IHttpActionResult DeleteEmployee(int Id)
        {
            empDb.DeleteEmployee(Id);
            return Ok();
        }

    }
}
