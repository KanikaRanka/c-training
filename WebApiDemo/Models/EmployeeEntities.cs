﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiDemo.Models
{
    public class EmployeeEntities
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string NAME { get; set; }
        [Required]
        public int AGE { get; set; }
        [Required]
        public string ADDRESS { get; set; }
        [Required]
        public int SALARY { get; set; }
    }
}