﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo8
{
    class Base
    {
        public Base()
        {
            Console.WriteLine("Base class Constructor");
        }
         static Base()
        {
            Console.WriteLine("Base class Static Constructor");
        }
        public void Start()
        {
            Console.WriteLine("Method In base");
        }

    }
   class Derived : Base
    {
        public Derived()
        {
            Console.WriteLine("Child class Constructor");
        }
        static Derived()
        {
            Console.WriteLine("Child class Static Constructor");
        }
        public void Start1()
        {
            Console.WriteLine("Method In Derived");
        }

    }
    class Derived2 : Base
    {
        public void Stop()
        {
            Console.WriteLine("Stop");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            
           Derived derived = new Derived();
           derived.Start1();
            Console.Read();
        }
    }
}
