﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_WebApi.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Core_WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        EmployeeDataAccessLayer empDb = new EmployeeDataAccessLayer();

        [HttpGet]
        public IActionResult GetAllEmployee()
        {

            return Ok(empDb.GetAllEmployees());

        }


        //public IEnumerable<Employee> GetAllEmployee()
        //{

        //    return empDb.GetAllEmployees();

        //}
        [HttpPost]
        public IActionResult PostAddEmployee(Employee modelObj)
        {
            empDb.AddEmployee(modelObj);
            return Ok();

        }
        [HttpGet("{Id:int}")]
        public IActionResult GetEditEmployee(int Id)
        {
            empDb.GetEmployeeData(Id);
            return Ok();
        }



        [HttpPut]
        public IActionResult PutUpdateEmployee(Employee modelObj)
        {
            empDb.UpdateEmployee(modelObj);
            return Ok();
        }
        [HttpDelete]
        public IActionResult DeleteEmployee(int Id)
        {
            empDb.DeleteEmployee(Id);
            return Ok();
        }

    }
}
