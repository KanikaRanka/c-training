﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo10
{

   class Program
   {
       

        enum Months { Jan=1, Feb, March, April, May, June, July, Aug, Sep, Oct, Nov, Dec };
        int i;
        public Program(int input)
        {
            i = input;
        }

        public void findDays()
        {
            Dictionary<int, int> Mydictionary =
                     new Dictionary<int, int> {
                         {(int)Months.Jan,31 },
                         {(int)Months.Feb,28 },
                         {(int)Months.March,31 },
                         {(int)Months.April,30 },
                         {(int)Months.May,31 },
                         {(int)Months.June,31 },
                         {(int)Months.July,31 },
                         {(int)Months.Aug,31 },
                         {(int)Months.Sep,31 },
                         {(int)Months.Oct,31 },
                         {(int)Months.Nov,31 },
                         {(int)Months.Dec,31 }

                     };

                //Mydictionary.Add(Months.Jan.ToString(), 31);
                //Mydictionary.Add(Months.Feb.ToString(), 28);
                //Mydictionary.Add(Months.March.ToString(), 31);
                //Mydictionary.Add(Months.April.ToString(), 30);
                //Mydictionary.Add(Months.May.ToString(), 31);
                //Mydictionary.Add(Months.June.ToString(), 30);
                //Mydictionary.Add(Months.July.ToString(), 31);
                //Mydictionary.Add(Months.Aug.ToString(), 31);
                //Mydictionary.Add(Months.Sep.ToString(), 30);
                //Mydictionary.Add(Months.Oct.ToString(), 30);
                //Mydictionary.Add(Months.Nov.ToString(), 30);
                //Mydictionary.Add(Months.Dec.ToString(), 30);


        foreach (KeyValuePair<int, int> month in Mydictionary)
            {
                if (month.Key == i)
                {
                    Console.WriteLine(month.Value);
                }
            }
               
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter month: ");
            int input = int.Parse(Console.ReadLine());
            Program program = new Program(input);
            program.findDays();
            Console.Read();
            

        }
   }
}

