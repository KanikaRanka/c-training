﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp2.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult IndexTestingShared()
        {
            TempData["Name"] = "Kanika Ranka";

            return RedirectToAction("Index1");
        }
        public ActionResult Index1()
        {

            ViewBag.data = TempData["Name"].ToString();

            return View("IndexTestingShared");
        }
    }
}