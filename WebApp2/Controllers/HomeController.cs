﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp2.Filters;
using WebApp2.Models;

namespace WebApp2.Controllers
{
    [AuthenticationFilter]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Name = "Welcome";
            return View("Index");
        }
        [HttpGet]
        public RedirectResult redirect()
        {
            return Redirect("About");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ContentResult Contact()
        {
            return Content("Hello this is Content Result Type");
        }

        public PartialViewResult _PartialPage1()
        {
            //for partial view    

            return PartialView();
        }
        
        [HttpPost]
        [ActionName("Index")]
        public ActionResult Index_Post()
        {
            if (ModelState.IsValid)
            {
                StudentModel student = new StudentModel();
                UpdateModel(student);
                ViewBag.SucessMessage = "Employee Info save" + "-" + student.StudentId;
                return View();
            }
            return View();

        }

        
    }
}