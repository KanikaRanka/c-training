﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp2.Controllers
{
    [RoutePrefix("Property")]
    public class PropertyController : Controller
    {
        // GET: Property
        [Route("Buy")]
        [HttpGet]
        public ActionResult Buy()
        {
            return View();
        }
        [Route("Sell")]
        public ActionResult Sell()
        {
            return View();
        }
        [Route("Rent")]
        public ActionResult Rent()
        {
            return View();
        }
    }
}