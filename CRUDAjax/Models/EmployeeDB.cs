﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CRUDAjax.Models
{
    public class EmployeeDB
    {
        SqlConnection con = new SqlConnection("Data Source=KANIKARANKAR091\\SQLEXPRESS2019;Initial Catalog=EMPLOYEE_DETAILS;User Id=sa;Password=Kanika*123;");

        public List<Employee> ListAll()
        {
            List<Employee> lst = new List<Employee>();
            
                con.Open();
                SqlCommand com = new SqlCommand("SelectEmployee", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Employee
                    {
                        ID = Convert.ToInt32(rdr["Id"]),
                        NAME = rdr["Name"].ToString(),
                        AGE = Convert.ToInt32(rdr["Age"]),
                        ADDRESS = rdr["State"].ToString(),
                        SALARY = Convert.ToInt32(rdr["Salary"]),
                    });
                }
                return lst;
        }
            public string AddEmployeeRecord(Employee employee)
            {

            try
            {
                SqlCommand cmd = new SqlCommand("sp_EmployeeAdd", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Id", employee.ID);
                cmd.Parameters.AddWithValue("@Name", employee.NAME);
                cmd.Parameters.AddWithValue("@Age", employee.AGE);
                cmd.Parameters.AddWithValue("@Address", employee.ADDRESS);
                cmd.Parameters.AddWithValue("@Salary", employee.SALARY);
                con.Open();
                int insertVal = cmd.ExecuteNonQuery();
                con.Close();
                return ("Data save Successfully");
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return (ex.Message.ToString());
            }
        }
        

        public IEnumerable<Employee> GetEmployeeEntities()
        {
            List<Employee> lstemployee = new List<Employee>();

            SqlCommand cmd = new SqlCommand("spGetAllEmployee", con);
            cmd.CommandType = CommandType.StoredProcedure;

            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Employee employee = new Employee();

                employee.ID = System.Convert.ToInt32(rdr["ID"]);
                employee.NAME = rdr["Name"].ToString();
                employee.AGE = System.Convert.ToInt32(rdr["Age"]);
                employee.ADDRESS = rdr["Address"].ToString();
                employee.SALARY = System.Convert.ToInt32(rdr["Salary"]);

                lstemployee.Add(employee);
            }
            con.Close();
            return lstemployee;
        }


        public Employee GetEmployeeData(int? id)
        {
            Employee employee = new Employee();


            string sqlQuery = "SELECT* FROM CUSTOMERS WHERE ID = " + id;
            SqlCommand cmd = new SqlCommand(sqlQuery, con);

            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                employee.ID = System.Convert.ToInt32(rdr["ID"]);
                employee.NAME = rdr["Name"].ToString();
                employee.AGE = System.Convert.ToInt32(rdr["Age"]);
                employee.ADDRESS = rdr["Address"].ToString();
                employee.SALARY = System.Convert.ToInt32(rdr["Salary"]);
            }

            return employee;
        }


        
        public int UpdateEmployee(Employee obj)
        {

            SqlCommand com = new SqlCommand("spUpdateEmployee", con);
            com.CommandType = CommandType.StoredProcedure;

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", obj.ID);
            com.Parameters.AddWithValue("@Name", obj.NAME);
            com.Parameters.AddWithValue("@Age", obj.AGE);
            com.Parameters.AddWithValue("@Address", obj.ADDRESS);
            com.Parameters.AddWithValue("@Salary", obj.SALARY);

            con.Open();
           int i= com.ExecuteNonQuery();
            con.Close();
            return i;

        }

        public int DeleteEmployee(int ID)
        {

            con.Open();
            SqlCommand cmd = new SqlCommand("delete from  CUSTOMERS where ID=@Id", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", ID);
            cmd.ExecuteNonQuery();
            con.Close();

            return ID;

        }

    }
}
