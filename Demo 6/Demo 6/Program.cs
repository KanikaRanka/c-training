﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_6
{
    abstract class Vehical
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }

        }
        private string color;
        public string Color
        {
            get { return color; }
            set { color = value; }

        }
        private int noOfWheels;
        public int NoOfWheels
        {
            get { return noOfWheels; }
            set { noOfWheels = value; }
        }
        public readonly int maxSpeed;

        public Vehical() { }



        public Vehical(string vehicalName, string vehicalColor, int vehicalWheels, int vehicalSpeed)
        {

            name = vehicalName;
            color = vehicalColor;
            noOfWheels = vehicalWheels;
            maxSpeed = vehicalSpeed;
            Console.WriteLine("In Vechile class");

        }

        //Abstract Method
        public abstract void CalTollAmt();

        public void SpeedUp(int speed)
        {
            speed = maxSpeed;
            Console.WriteLine(speed);

        }
        public void Start()
        {
            Console.WriteLine("Vehical Stated!");
        }

        public static void Stop()
        {
            Console.WriteLine("Vehical Stoped!");
        }
        public void DisplayProperties()
        {
            Console.WriteLine("NAME :" + name + " " + "Color: " + color + " "
                + "No of wheels :" + noOfWheels + " " + "Speed :" + maxSpeed);
        }
        
    }

    sealed class Car : Vehical
    {
        string seater;
        public Car(string carName, string carColor, int carWheels, int carSpeed, string s) : base(carName, carColor, carWheels, carSpeed)
        {
            seater = s;
            Console.WriteLine("In Car Class");
        }
        public Car() { }
        public void DisplaySeater()
        {
            Console.WriteLine("Available seat: " + seater);
        }
        public override void CalTollAmt()
        {
            int amt = 10 + maxSpeed;
            //Console.WriteLine("In Car" + maxSpeed);
            Console.WriteLine("Car toll Amt :" + amt);
        }
    }

    sealed class Bike : Vehical
    {
        public Bike() { }
        public Bike(string bikeName, string bikeColor, int bikeWheels, int bikeSpeed) : base(bikeName, bikeColor, bikeWheels, bikeSpeed)
        {

        }
        public override void CalTollAmt()
        {
            int amt = 5 + maxSpeed;
            // Console.WriteLine("In Bike" + maxSpeed);
            Console.WriteLine("Bike toll Amt :" + amt);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Car Object   
            Car car1 = new Car("Ford", "Black", 5, 100, "two");
            Console.WriteLine("Car Properties");
            car1.Start();
            car1.DisplaySeater();
            car1.DisplayProperties();
            car1.CalTollAmt();
            Vehical.Stop();    //static method can't be called by instance
            Console.WriteLine();

            //Bike Object
            Bike bike = new Bike("KTM", "White", 2, 250);
            Console.WriteLine("Bike Properties");
            bike.DisplayProperties();
            bike.CalTollAmt();
            Console.WriteLine();

            //Vehical object
            Car vehicle = new Car();
            vehicle.Start();
            vehicle.CalTollAmt();
            
            
            Console.WriteLine();

            Console.ReadKey();

        }

    }

}
