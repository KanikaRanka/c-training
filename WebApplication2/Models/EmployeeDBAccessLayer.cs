﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class EmployeeDBAccessLayer
    {
        SqlConnection con = new SqlConnection("Data Source=KANIKARANKAR091\\SQLEXPRESS2019;Initial Catalog=EMPLOYEE_DETAILS;User Id=sa;Password=Kanika*123;");
        public string AddEmployeeRecord(EmployeeEntities employeeEntities)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_EmployeeAdd", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Id", employeeEntities.ID);
                cmd.Parameters.AddWithValue("@Name", employeeEntities.NAME);
                cmd.Parameters.AddWithValue("@Age", employeeEntities.AGE);
                cmd.Parameters.AddWithValue("@Address", employeeEntities.ADDRESS);
                cmd.Parameters.AddWithValue("@Salary", employeeEntities.SALARY);
                con.Open();
                int insertVal = cmd.ExecuteNonQuery();
                con.Close();
                return ("Data save Successfully");
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return (ex.Message.ToString());
            }
        }
        //public List<EmployeeEntities> GetEmployeeEntitie1s()
        //{

        //    List<EmployeeEntities> EmpList = new List<EmployeeEntities>();


        //    SqlCommand com = new SqlCommand("sp_GetEmployees", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    SqlDataAdapter da = new SqlDataAdapter(com);
        //    DataTable dt = new DataTable();

        //    con.Open();
        //    da.Fill(dt);
        //    con.Close();
        //    //Bind EmpModel generic list using dataRow     
        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        EmpList.Add(

        //            new EmployeeEntities
        //            {

        //                ID = Convert.ToInt32(dr["Id"]),
        //                NAME = Convert.ToString(dr["Name"]),
        //                AGE = Convert.ToInt32(dr["Age"]),
        //                ADDRESS = Convert.ToString(dr["Address"]),
        //                SALARY = Convert.ToInt32(dr["Salary"]),

        //            }
        //            );
        //    }
        //    return EmpList;
        //}

        public IEnumerable<EmployeeEntities> GetEmployeeEntities()
        {
            List<EmployeeEntities> lstemployee = new List<EmployeeEntities>();

            SqlCommand cmd = new SqlCommand("spGetAllEmployee", con);
            cmd.CommandType = CommandType.StoredProcedure;

            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                EmployeeEntities employee = new EmployeeEntities();

                employee.ID = System.Convert.ToInt32(rdr["ID"]);
                employee.NAME = rdr["Name"].ToString();
                employee.AGE = System.Convert.ToInt32(rdr["Age"]);
                employee.ADDRESS = rdr["Address"].ToString();
                employee.SALARY = System.Convert.ToInt32(rdr["Salary"]);

                lstemployee.Add(employee);
            }
            con.Close();
            return lstemployee;
        }


        public EmployeeEntities GetEmployeeData(int? id)
        {
            EmployeeEntities employee = new EmployeeEntities();

            
                string sqlQuery = "SELECT* FROM CUSTOMERS WHERE ID = " + id;
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    employee.ID = System.Convert.ToInt32(rdr["ID"]);
                    employee.NAME = rdr["Name"].ToString();
                    employee.AGE = System.Convert.ToInt32(rdr["Age"]);
                    employee.ADDRESS = rdr["Address"].ToString();
                    employee.SALARY = System.Convert.ToInt32(rdr["Salary"]);
            }
            
            return employee;
        }


        //public bool UpdateEmployee(EmployeeEntities obj)
        //{
        //    SqlCommand com = new SqlCommand("spUpdateEmployee", con);

        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.AddWithValue("@Id", obj.ID);
        //    com.Parameters.AddWithValue("@Name", obj.NAME);
        //    com.Parameters.AddWithValue("@Age", obj.AGE);
        //    com.Parameters.AddWithValue("@Address", obj.ADDRESS);
        //    com.Parameters.AddWithValue("@Salary", obj.SALARY);
        //    con.Open();
        //    int i = com.ExecuteNonQuery();
        //    con.Close();
        //    if (i >= 1)
        //    {

        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public void UpdateEmployee(EmployeeEntities obj)
        {
           
                SqlCommand com = new SqlCommand("spUpdateEmployee", con);
                com.CommandType = CommandType.StoredProcedure;

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", obj.ID);
            com.Parameters.AddWithValue("@Name", obj.NAME);
            com.Parameters.AddWithValue("@Age", obj.AGE);
            com.Parameters.AddWithValue("@Address", obj.ADDRESS);
            com.Parameters.AddWithValue("@Salary", obj.SALARY);

                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            
        }

        public void DeleteEmployee(int ID)
        {

            con.Open();
            SqlCommand cmd = new SqlCommand("delete from  CUSTOMERS where ID=@Id", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", ID);
            cmd.ExecuteNonQuery();
            con.Close();
            //SqlCommand cmd = new SqlCommand("spDeleteEmployee", con);
            //    cmd.CommandType = CommandType.StoredProcedure;

            //    cmd.Parameters.AddWithValue("@ID", ID);

            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            
        }

    }
}