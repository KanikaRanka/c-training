﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Questions
    {
        [Required(ErrorMessage = "Please enter your Favourite Fruit.")]
        [Display(Name = "Favourite Fruit : ")]
        public string Fruit { get; set; }

        [Required(ErrorMessage = "Please enter your Favourite Day.")]
        [Display(Name = "Favourite Day : ")]
        public string Day { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Please enter your Email Address.")]
        [Display(Name = "EmailAddress : ")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please enter your Favourite Color.")]
        [Display(Name = "Favourite Color : ")]
        public string Color { get; set; }

        public string strFruit { get; set; }
        public string strDay { get; set; }
        public string strEmailAddress { get; set; }
        public string strColor { get; set; }
    }
}
