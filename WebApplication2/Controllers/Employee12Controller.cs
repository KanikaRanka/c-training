﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class Employee12Controller : Controller
    {
        // GET: Employee12
        EmployeeDBAccessLayer empdb = new EmployeeDBAccessLayer();
        private EmployeeEntities obj;

        public ActionResult Index()
        {
            List<EmployeeEntities> lstEmployee = new List<EmployeeEntities>();
            lstEmployee = empdb.GetEmployeeEntities().ToList();
            return View(lstEmployee);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            EmployeeEntities employee = empdb.GetEmployeeData(id);

            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind]EmployeeEntities employee)
        {
            if (id != employee.ID)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                empdb.UpdateEmployee(employee);
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        private ActionResult NotFound()
        {
            
            throw new NotImplementedException("Not Found");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetAllEmpDetails()
        {

            EmployeeDBAccessLayer EmpRepo = new EmployeeDBAccessLayer();
            ModelState.Clear();
            return View(EmpRepo.GetEmployeeEntities());

        }
        public ActionResult Create(EmployeeEntities employeeEntities)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string resp = empdb.AddEmployeeRecord(employeeEntities);
                    TempData["msg"] = resp;
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = ex.Message;
            }
            return View();
        }
        
        public ActionResult Delete(int ID)
        {
            
            empdb.DeleteEmployee(ID);
            return RedirectToAction("Index");
        }
       

    }
}
