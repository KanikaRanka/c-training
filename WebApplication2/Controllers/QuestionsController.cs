﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class QuestionsController : Controller
    {
        // GET: Questions
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Question()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Question(Questions questions)
        {
            if (ModelState.IsValid)
            {
                Response.Cookies.Add(new HttpCookie("Day", questions.Day));
                Response.Cookies.Add(new HttpCookie("Color", questions.Color));
                Response.Cookies.Add(new HttpCookie("EmailAddress", questions.EmailAddress));
                Response.Cookies.Add(new HttpCookie("Fruit", questions.Fruit));
                //RedirectToAction("MyAnswers");
                return RedirectToAction("MyAnswers", "Questions");
            }

            return View(questions);
        }

        public ActionResult MyAnswers()
        {
            return View();
        }

    }

}
