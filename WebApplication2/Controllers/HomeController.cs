﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Common;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        [TrackExecutionTime]
        public string Index1()
        {
            return "Index Action Invoked";
        }

        [TrackExecutionTime]
        public string Welcome()
        {
            throw new Exception("Exception ocuured");
        }
        public ActionResult Index()
        {
            PersonModel person = new PersonModel
            {
                Name = "Kanika"
            };

            ViewBag.Country = "India";
            return View(person);
        }

        [HttpPost]
        public ActionResult Index(PersonModel person)
        {
            string name = person.Name;
            string country = Request.Form["Country"];
            return View();
        }
        // GET: Home
        public ActionResult QueryTest()
        {
            string name = "Kanika";
            if (!String.IsNullOrEmpty(HttpContext.Request.QueryString["name"]))
                name = HttpContext.Request.QueryString["name"];

            return Content("Name from query string: " + name);
        }
        public ActionResult NonSecureMethod()
        {
            return View();
        }
        [Authorize]
        public ActionResult SecureMethod()
        {
            return View();
        }
    }
}