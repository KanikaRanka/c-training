﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class Circle {
        double perCir, areaCir;
        double PI = 3.14;

        public void Perimeter(double radius) {

            perCir = 2 * PI * radius;
            Console.WriteLine("Perimeter of Circle : {0}", perCir);
            
        }

        public void Area(double radius) {
            areaCir = PI * radius * radius;
            Console.WriteLine("Area of Circle: {0}", areaCir);
           

        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            double radius;
            Console.WriteLine("Input the radius of the circle : ");
            radius= Convert.ToDouble(Console.ReadLine());
            Circle circle = new Circle();
            circle.Perimeter(radius);
            circle.Area(radius);

            Console.ReadKey();

        }
    }
}
