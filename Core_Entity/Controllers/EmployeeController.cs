﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_Entity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_Entity.Controllers
{
    public class EmployeeController : Controller
    {
        DataContext emp = new DataContext();
        public IActionResult Index()
        {
            var data = emp.Employees.ToList();         
            return View(data);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee model)
        {
            if (ModelState.IsValid)
            {
                emp.Employees.Add(model);
                emp.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            Employee employeedetail = emp.Employees.Find(id);

            return View(employeedetail);

        }
        [HttpPost]

        public ActionResult Edit(Employee employeedetail)
        {
            if (ModelState.IsValid)
            {
                emp.Entry(employeedetail).State = EntityState.Modified;
                emp.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employeedetail);
        }
        public ActionResult Details(int id)
        {
            Employee employeedetail = emp.Employees.Find(id);
            if (employeedetail == null)
            {
               
            }
            return View(employeedetail);
        }

        public ActionResult Delete(int id = 0)
        {
            Employee employeedetail = emp.Employees.Find(id);
            
            return View(employeedetail);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employeedetail = emp.Employees.Find(id);
            emp.Employees.Remove(employeedetail);
            emp.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}