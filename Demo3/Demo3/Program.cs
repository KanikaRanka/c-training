﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    interface IPainter
    {
        void Paint();
    }
     class Vehical : IPainter
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }

        }
        private string color;
        public string Color
        {
            get { return color; }
            set { color = value; }

        }
        public enum Wheels
        {
            carWheels =4 ,
            bikeWheels=2
        };
        
        //private int noOfWheels;
        //public int NoOfWheels {
        //    get { return noOfWheels; }
        //    set { noOfWheels = value; }
        //}
        public readonly int maxSpeed ;

        public Vehical() { }
        
        

        public Vehical(string vehicalName, string vehicalColor, int vehicalSpeed ) {

            var carEnum = Wheels.carWheels;
            var bikeEnum = Wheels.bikeWheels;

            name = vehicalName;
            color = vehicalColor;
            
            maxSpeed = vehicalSpeed;
            Console.WriteLine("In Vechile class");
            
        }


        public void Paint()
        {
            Console.WriteLine("Painted");
            
        }

        public void SpeedUp(int speed) {
            speed = maxSpeed;
            Console.WriteLine(speed);

        }
        public void Start()
        {
            Console.WriteLine("Vehical Stated!");
        }

        public static void Stop()
        {
            Console.WriteLine("Vehical Stoped!");
        }
        public void DisplayProperties()
        {
            Console.WriteLine("NAME :" + name +" " + "Color: " + color + " " 
                + "No of wheels :" + Wheels.carWheels +" "+ "Speed :" + maxSpeed);
        }

        
    }
     interface IChangeCover
    {
        void ChangeSeatCover();
    }
   sealed class Car : Vehical , IChangeCover  {
        string seater;
        public Car(string carName, string carColor, int carSpeed, string s) : base(carName,carColor,carSpeed)
        {
            seater = s;
            Console.WriteLine("In Car Class");
        }
       public Car() { }
        public void DisplaySeater()
        {
            Console.WriteLine("Available seat: " + seater);
        }
        public void CalTollAmt()
        {
            int amt = 10 + maxSpeed;
            //Console.WriteLine("In Car" + maxSpeed);
            Console.WriteLine("Car toll Amt :" + amt);
        }

        public void ChangeSeatCover()
        {
            Console.WriteLine("Car seat cover changed");
            
        }
    }

    sealed class Bike : Vehical, IChangeCover {
       public Bike() { }
        public Bike(string bikeName, string bikeColor,  int bikeSpeed): base(bikeName,bikeColor,  bikeSpeed)
        {

        }
        public void CalTollAmt()
        {
            int amt = 5 + maxSpeed;
           // Console.WriteLine("In Bike" + maxSpeed);
            Console.WriteLine("Bike toll Amt :" + amt);
        }

        public void ChangeSeatCover()
        {
            Console.WriteLine("Bike seat cover changed");
            
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Vehical object
            Vehical truck = new Vehical();
            truck.Name = "Truck";
            truck.Color = "Brown";
            Console.WriteLine("Truck Properties");
            Console.Write(truck.Name + " ");
            Console.Write(truck.Color + " ");
            Console.Write(truck.maxSpeed + " ");
            Console.WriteLine();
            truck.Paint();

            //Car Object   
            Car car1 = new Car("Ford", "Black", 100,"two");           
            Console.WriteLine("Car Properties");
            car1.Start();
            car1.DisplaySeater();
            car1.DisplayProperties();
            car1.ChangeSeatCover();          
            car1.Paint();
            
            car1.CalTollAmt();
            Vehical.Stop();    //static method can't be called by instance
            Console.WriteLine();

            //Bike Object
            Bike bike = new Bike("KTM","White",  250);
            Console.WriteLine("Bike Properties");
            bike.DisplayProperties();
            bike.CalTollAmt();
            bike.ChangeSeatCover();
            Console.WriteLine();
                            
            Console.ReadKey();

        }
        
    }

}
