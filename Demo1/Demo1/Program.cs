﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int input;
            string operation;


            try
            {

                Console.WriteLine("Input first number: ");
                input = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Choose the operation to be performed");
                Console.WriteLine("1. Char");
                Console.WriteLine("2. Decimal");
                Console.WriteLine("3. Double");
                Console.WriteLine("4. Int");
                Console.WriteLine("5. Exit");
                operation = Console.ReadLine();


                if (operation == "1")
                    Console.WriteLine("After Conversion: {0}  ", Convert.ToChar(input));

                else if (operation == "2")
                    Console.WriteLine("After Conversion: {0}  ", Convert.ToDecimal(input));
                else if ((operation == "3"))
                    Console.WriteLine("After Conversion: {0}  ", Convert.ToDouble(input));
                else if (operation == "4")
                    Console.WriteLine("After Conversion: {0}  ", Convert.ToInt32(input));
                else if (operation == "5") {
                    System.Environment.Exit(0);
                }
                else
                    Console.WriteLine("Wrong Input");
                Console.ReadKey();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }
    }
}
