﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo5
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] months = { "Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" };

            for (int i = 0; i < months.Length; i++)
            {
                if (months[i][0] == 'J')
                {
                    Console.WriteLine(months[i]);

                }
            }
            Console.Read();
      //    usingWhile();

             usingForEach();
        }


        static void usingWhile()
        {
            string[] months = { "Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" };

            int i = 0;

            while (i < months.Length)
            {
                if (months[i][0] == 'J')
                {
                    Console.WriteLine(months[i]);
                    i++;
                }
            }
            Console.Read();
        }
    static void usingForEach() 
        {
            string[] months = { "Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" };
            foreach (string s in months)
            {
                if (s.StartsWith("J"))
                {
                    Console.WriteLine(s);
                }
            }
            Console.Read();
        }
    }

}