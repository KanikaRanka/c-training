﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9
{
    class Person
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private int age;
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        //public Person(string perName, int perAge)
        //{
        //    name = perName;
        //    age = perAge;
        //}
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> PersonList = new List<Person>
            {
                new Person {Name = "Kanika",Age =22 },
                new Person {Name = "Anika", Age = 55},
                new Person {Name = "Sonika", Age = 65},
                new Person {Name = "Sarika", Age = 15},
                new Person {Name = "Vanika", Age = 35}
        };
            
            //PersonList.Add(new Person("Kanika", 22));
            //PersonList.Add(new Person("Anika", 25));
            //PersonList.Add(new Person("Vanika", 32));
            //PersonList.Add(new Person("Sonika", 12));
            //PersonList.Add(new Person("Sarika", 42));

            foreach(var person in PersonList)
            {
                if (person.Age >= 25)
                {
                    Console.WriteLine("Name: " + person.Name + " " + "Age: " + person.Age);
                }
            }
            Console.Read();
        }
    }
}
